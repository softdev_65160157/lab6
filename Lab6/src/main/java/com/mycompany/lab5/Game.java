/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Table table;
    private int turnCount;
    private Player player1, player2;
    private boolean start;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void newGame() {
        table = new Table(player1, player2);
    }

    public void Play() {

        start = true;
        showWelcome();
        newGame();
        while (start) {
            showTable();
            showTurn();
            inputNum();
            if (table.getCheck()) {
                if (table.isWin()) {
                    turn();
                    System.out.println();
                    System.out.println(table.getCurrentPlayer().getSymbal() + "  Win");
                    saveStat();
                    newGame();
                    inputContinue();

                } else if (table.isDraw()) {
                    turn();
                    System.out.println("Draw");
                    saveDraw();
                    newGame();
                    inputContinue();
                } else {
                    table.changePlayer();
                }
            }
        }

        printScore();
    }

    public void showWelcome() {

        System.out.println("Welcome to OX Game");
    }

    private void showTable() {
        char[][] t = table.getTable();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " ");
            }
            System.out.println();
        }

    }

    private void showTurn() {
        System.out.println();
        System.out.println(table.getCurrentPlayer().getSymbal() + " Turn");
    }

    private void inputNum() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input num : ");
        char num = sc.next().charAt(0);
        table.setNum(num);
    }

    public void inputContinue() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Do you want to continue (yes or no) : ");
        String Continue = sc.nextLine();
        System.out.println();
        newGame();

        if (Continue.equals("yes")) {
            newGame();
            start = true;
        } else {
            start = false;
        }
    }

    private void saveStat() {

        if (table.getCurrentPlayer() == player1) {
            player1.win();
            player2.lose();
        } else {
            player1.lose();
            player2.win();
        }

    }

    private void saveDraw() {

        if (table.getCurrentPlayer() == player1) {
            player1.draw();
            player2.draw();
        }

    }

    public void turn() {
        turnCount++;
    }

    public int getTurnCount() {
        return turnCount;
    }

    private void printScore() {
        System.out.println("Turn " + getTurnCount());
        System.out.println();
        System.out.println(player1.getSymbal());
        System.out.println("Win " + player1.getWinCount());
        System.out.println("Lose" + player1.getLoseCount());
        System.out.println("Draw " + player1.getDrawCount());
        System.out.println();
        System.out.println(player2.getSymbal());
        System.out.println("Win " + player2.getWinCount());
        System.out.println("Lose " + player2.getLoseCount());
        System.out.println("Draw " + player2.getDrawCount());
    }

}
