/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

/**
 *
 * @author informatics
 */
public class TestWritePlayer {

    public static void main(String[] args) throws FileNotFoundException, IOException {
        Player player1  = new Player('X');
        Player player2  = new Player('O');

        System.out.println(player1 );
        System.out.println(player2);
        File file = new File("players.dat");
        FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream ois = new ObjectOutputStream(fos);
            ois.writeObject(player1 );
            ois.writeObject(player2 );
            ois.close();
            fos.close();
        }

    }


